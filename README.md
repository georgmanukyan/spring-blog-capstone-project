# Java Spring Boot Blog
A simple blog application capstone project.

- Implementing Materialize CSS Framework
- Spring Boot 2.1.x
- Hibernate ORM + JPA
- Thymeleaf
- MySQL

### First steps

1. Clone this repo locally.
2. Install Docker and Docker Compose.
3. Run docker pull mysql/mysql-server:latest.
4. Run docker run -p 3306:3306 --name mysql1 -e MYSQL_ROOT_PASSWORD=root -d mysql --default-authentication-plugin=mysql_native_password -h 127.0.0.1 
5. Create an `application.properties` file with valid credentials for a MySQL connection, use the `application.properties.example` as a template.
6. Configure the `application.properties` file with your MySQL connection credentials.
7. Add any API keys needed to test the full functionality.
8. If you want to use the seeder file leave the `seed.db=true` property, change it to: `seed.db=false` or any value that makes sense to you when you don't want to seed the database.

User can register, login, logout, create posts, edit posts, delete posts, and view posts.
While creating a post, user have to add tags to the post.