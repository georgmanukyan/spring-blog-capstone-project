package com.gevorg.blog.models;

public enum FriendStatus {
    PENDING,
    ACCEPTED,
    REJECTED
}
